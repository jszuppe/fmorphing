
Created by `Jakub Szuppe <https://bitbucket.org/jszuppe>`_

=========
fmorphing
=========

Simple one-line image morphing/wraping function in amd64 assembly and an example in C with allegro (4.4.2).

1) Linear interpolation
2) Using red-zone (it's a leaf function)
3) AMD64 API (Linux 64-bit)
4) bmp files

Funkcja fmorphing, która transformuje obrazek względem jednej prostej. 

1) Interpolacja liniowa
2) Użycie red-zone (patrz AMD64)
3) Zgodne z API dostępnym w manualu AMD64 (Linux 64-bit)
