section .data

CONST1:		dq 1.0
CONST2:		dq 1.0e-9

section	.text

global	fmorphing

fmorphing:

	push 		rbp
	mov			rbp, rsp	
	;~ sub			rsp, 256
	
	push		rbx
	push 		r12
	push 		r13
	push 		r14
	push 		r15
	
	;~ Prosta: y = _a * x 
	;~ Punkt (0,0) przyjmuje w (_b, 0)
	;~ x = _b + _y/_a
	
	;~ ***************** ARGUMENTY *******************
	
	mov			dword [rbp-24], edi	; imgw
	mov			dword [rbp-28], esi ; imgh
	mov			qword [rbp-40], rdx ; ptr_source
	mov			qword [rbp-48], rcx	; ptr_copy
	movsd 		qword [rbp-56], xmm0; _a
	movsd 		qword [rbp-64], xmm1; _b
		
	%idefine 	line_nr				[rbp-20]		
	%idefine	imgw				[rbp-24]	
	%idefine 	imgh				[rbp-28]	
	%idefine	ptr_source 			[rbp-40]
	%idefine	ptr_copy 			[rbp-48]

	%idefine	_a					[rbp-56]
	%idefine	_b					[rbp-64]		
	;~ ***********************************************
	
	%idefine	ptr_source_line 	[rbp-120]
	%idefine	ptr_copy_line 		[rbp-128]
	
	;~ mov			esi, dword imgh
	mov			dword line_nr, esi
	sub			dword line_nr, 1
		
	jmp	L0_condition
	;~ **************** PETLA L0 *********************	
L0:
	
	mov	rax, qword ptr_source
	mov	edx, dword line_nr
	;~ movsxd	rdx, edx
	;~ add	rdx, 12
	mov	rax, qword [rax+rdx*8]
	mov	qword ptr_source_line, rax
	
	mov	rax, qword ptr_copy
	mov	edx, dword line_nr
	;~ movsxd	rdx, edx
	;~ add	rdx, 12
	mov	rax, qword [rax+rdx*8]
	mov	qword ptr_copy_line, rax
	
	;~ ***************** LICZYMY BORDER ***************
	;~ 
	;~ double border = _b + ((double)(imgh-line_nr-1)/_a);		
	%idefine 	border [rbp-72]
				
	mov			esi, imgh 	
	sub			esi, line_nr
	sub			esi, 1

	cvtsi2sd	xmm0, esi			; (double)(imgh-line_nr-1)
	divsd		xmm0, qword _a		; ((double)(imgh-line_nr-1)/_a)
	addsd		xmm0, qword _b	; _b + ((double)(imgh-line_nr-1)/_a)	
	movsd		qword border, xmm0	
	
	;~ ***********************************************
		
	;~ ***************** LICZYMY copy_x_end **********
	%idefine	copy_x_end [rbp-76]
	
	cvtsd2si	edx, xmm0	
	lea			edx, [edx * 3]
	mov			eax, imgw
	lea			eax, [eax * 3]	
	
	cmp			edx, eax
	cmovle		eax, edx
	mov			dword copy_x_end, eax
	;~ ***********************************************	
	
	;~ ***************** LICZYMY RATIO ***************	
	%idefine	ratio [rbp-88]
	
	movsd		xmm0, qword border
	divsd		xmm0, qword _b
	movsd 		qword ratio, xmm0	
	;~ ***********************************************
	
	;~ ********** PRZYGOTOWANIE DO PETLI L1 **********	
	
	;~ %idefine		b [rbp-80]
	;~ %idefine 	g [rbp-92]
	;~ %idefine 	r [rbp-96]
	;~ %idefine prevcopyshare [rbp-104]
	
	;~ r8	- ratio
	;~ r9	- prevcopyshare
	;~ r10	- ptr_source_image	
	;~ r11	- ptr_copy_image
	;~ r12b	- b		
	;~ r13b	- g
	;~ r14b	- r FAD
	;~ r15	- copy_x_end+ptr_copy_image
	
	mov			r8,		qword ratio	
	mov			r9, 	r8	
	mov			r10,	ptr_source_line
	mov			r11, 	ptr_copy_line	
	
	movzx		r12d,	byte [r10]
	movzx		r13d,	byte [r10+1]
	movzx		r14d,	byte [r10+2]
	add			r10, 	3
	
	mov 		r15d,	dword copy_x_end
	add			r15,	qword ptr_copy_line
	
	;~ ***********************************************	
	
	;~ **************** PETLA L1 *********************
	jmp	L1_condition
	
L1:
L1_if:
	;~ if(prevcopyshare >= 1.0)	
	movq		xmm0, r9
	movq		xmm1, qword [CONST1]
	ucomisd		xmm0, xmm1
	setae		al
	test		al, al
	;~ Nie jest spełnione, jump do else if
	je	L1_ifelse
	
	;~ Warunek spełniony, więc:
	;~ prevcolorshare -= 1.0;
	;~ movq		xmm0, r9				; chyba nie potrzebne, SPRAWDZIĆ!
	;~ movq		xmm1, qword [CONST1]	; chyba nie potrzebne, SPRAWDZIĆ!	
	subsd		xmm0, xmm1
	movq		r9, xmm0
	
	;~ Zmiana piksela w kopii, po kolei jego wartosci: b, g, r
	mov			byte [r11]	,	r12b
	mov			byte [r11+1],	r13b
	mov			byte [r11+2],	r14b
	
	;~ nastepna iteracja
	jmp	L1_iter
		
L1_ifelse:	
	;~ else if (prevcolorshare < 1E-9)	
	movq		xmm0, r9
	movq		xmm1, qword [CONST2]
	ucomisd		xmm1, xmm0
	seta		al
	test		al, al
	;~ Nie jest spełnione, jump do else
	je	L1_else
	
	;~ Spełnione, robimy:
	;~ Kolor z nastepnego piksela ze zrodła
	movzx		r12d,	byte [r10]
	movzx		r13d,	byte [r10+1]
	movzx		r14d,	byte [r10+2]
	add			r10 ,	3
	
	;~ Zmiana piksela w kopii, po kolei jego wartosci: b, g, r
	mov			byte [r11]	,	r12b
	mov			byte [r11+1],	r13b
	mov			byte [r11+2],	r14b	
	
	;~ prevcolorshare = ratio - 1.0;
	movq		xmm0,	r8
	movsd		xmm1,	qword [CONST1]
	subsd		xmm0,	xmm1
	movq		r9,		xmm0		
	
	;~ nastepna iteracja
	jmp	L1_iter
	
L1_else:	
	;~ else
	
	;~ (double)b	
	cvtsi2sd	xmm0,	r12d
	movq		xmm1,	r9
	;~ (double)b * prevcolorshare
	mulsd		xmm0,	xmm1	
	;~ (double)ptr1[source_x]
	movzx		eax,	byte [r10]
	cvtsi2sd	xmm2,	eax
	;~ (1.0 - prevcolorshare)
	movsd		xmm3,	qword [CONST1]
	subsd		xmm3,	xmm1
	;~ (double)ptr1[source_x] * (1.0 - prevcolorshare)
	mulsd		xmm3, 	xmm2
	;~ (double)b * prevcolorshare + (double)ptr1[source_x] * (1.0 - prevcolorshare)
	addsd		xmm0,	xmm3
	cvttsd2si	eax,	xmm0
	movzx		r12d,	byte al		
	
	;~ g	
	cvtsi2sd	xmm0,	r13d
	;~ movq		xmm1,	r9	; Chyba niepotrzebne
	mulsd		xmm0,	xmm1
	movzx		eax,	byte [r10+1]
	cvtsi2sd	xmm2,	eax	
	movsd		xmm3,	qword [CONST1]
	subsd		xmm3,	xmm1	
	mulsd		xmm3, 	xmm2	
	addsd		xmm0,	xmm3
	cvttsd2si	eax,	xmm0
	movzx		r13d,	byte al	
	
	;~ r
	cvtsi2sd	xmm0,	r14d
	;~ movq		xmm1,	r9	; Chyba niepotrzebne
	mulsd		xmm0,	xmm1	
	movzx		eax,	byte [r10+2]
	cvtsi2sd	xmm2,	eax	
	movsd		xmm3,	qword [CONST1]
	subsd		xmm3,	xmm1	
	mulsd		xmm3, 	xmm2	
	addsd		xmm0,	xmm3
	cvttsd2si	eax,	xmm0
	movzx		r14d,	byte al		
		
	;~ Zmiana piksela w kopii, po kolei jego wartosci: b, g, r
	mov			byte [r11],		r12b
	mov			byte [r11+1],	r13b
	mov			byte [r11+2],	r14b	
	
	;~ Kolor z nastepnego piksela ze zrodła
	movzx		r12d,	byte [r10]
	movzx		r13d,	byte [r10+1]
	movzx		r14d,	byte [r10+2]
	add			r10,	3
	
	;~ prevcolorshare = ratio + prevcolorshare - 1.0;
	movq		xmm0,	r8
	;~ movq		xmm1,	r9	; Chyba niepotrzebne
	movsd		xmm2,	qword [CONST1]
	subsd		xmm1,	xmm2
	addsd		xmm0,	xmm1
	movq		r9,		xmm0
	
L1_iter:
	;~ przejscie do nastepnego piksela
	add			r11, 3
	
L1_condition:
	;~ czy doszlismy do granicy (wyznaczonej przez prostą)
	cmp			r11, r15
	jl	L1
	;~ ************** KONIEC PETLI L1 ****************	
	
	
	;~ ***************** LICZYMY RATIO ***************		
	;~ ratio = ((double)(imgw) - border)/((double)(imgw - _b));
	;~ (double)(imgw) - border)	
	mov			eax,	dword imgw
	cvtsi2sd	xmm0,	eax	
	movapd		xmm2,	xmm0
	movsd		xmm1,	qword border
	subsd		xmm0,	xmm1
	;~ (double)(imgw - _b)	
	movsd		xmm3,	_b
	subsd		xmm2,	xmm3
	;~ ((double)(imgw) - border)/((double)(imgw - _b))
	divsd		xmm0,	xmm2	

	movsd 		qword ratio, xmm0
	
	;~ movsd		xmm0, qword [TEST1]
	
	;~ ***********************************************
	
	;~ ********** PRZYGOTOWANIE DO PETLI L2 **********	
	;~ iterujemy teraz po ptr_source_image az do konca obrazka
	
	;~ xmm0	- ratio
	;~ xmm1	- prevcopyshare
	;~ r10	- ptr_source_line
	;~ r11	- ptr_copy_line
	;~ r12b	- b		
	;~ r13b	- g
	;~ r14b	- r 
	;~ r15	- ptr_source_line_END = ptr_source_linge + imgw*3
	;~ xmm5 - double_b
	;~ xmm6 - double_g
	;~ xmm7	- double_r
	
	;~ ptr_source_image_END
	;~ w eax jest jux imgw	
	lea			eax,	[eax + eax*2]	
	mov 		r15d,	eax
	add			r15,	qword ptr_source_line
	
	;~ mov			r8,		qword ratio	
	;~ mov			r9, 	r8		
	movapd		xmm1,	xmm0
	
	;~ w xmm0 jest ratio
	;~ double_b
	movzx		r12,	byte [r10]
	cvtsi2sd	xmm5,	r12
	mulsd		xmm5,	xmm0
	;~ double_g
	movzx		r13,	byte [r10+1]
	cvtsi2sd	xmm6,	r13
	mulsd		xmm6,	xmm0
	;~ double_r
	movzx		r14,	byte [r10+2]
	cvtsi2sd	xmm7,	r14
	mulsd		xmm7,	xmm0
	add			r10, 	3
		
	jmp L2_condition
	;~ ***********************************************	
	
	;~ **************** PETLA L2 *********************
L2:
		
L2_if:
	;~ prevcolorshare + ratio >= 1.0
	movapd		xmm2,	xmm0
	addsd		xmm2,	xmm1	
	movq		xmm3,	qword [CONST1]
	ucomisd		xmm2,	xmm3
	setae		al
	test		al,		al
	;~ Nie jest spełnione, jump do else if
	je	L2_else
	
	;~ Warunek spełniony, więc:
	
	;~ double_b
	;~ double_b = double_b + (double)ptr1[source_x] * (1.0 - prevcolorshare);
	;~ (double)ptr1[source_x]
	movzx		r12,	byte [r10]
	cvtsi2sd	xmm2,	r12
	;~ (1.0 - prevcolorshare)
	movq		xmm3,	qword [CONST1]
	subsd		xmm3, 	xmm1
	;~ (double)ptr1[source_x] * (1.0 - prevcolorshare)
	mulsd		xmm2,	xmm3
	;~ double_b + (double)ptr1[source_x] * (1.0 - prevcolorshare)
	addsd		xmm5,	xmm2
	
	;~ double_g
	movzx		r13,	byte [r10+1]
	cvtsi2sd	xmm2,	r13	
	movq		xmm3,	qword [CONST1]
	subsd		xmm3, 	xmm1	
	mulsd		xmm2,	xmm3	
	addsd		xmm6,	xmm2
	;~ double_r
	movzx		r14,	byte [r10+2]
	cvtsi2sd	xmm2,	r14	
	movq		xmm3,	qword [CONST1]
	subsd		xmm3, 	xmm1	
	mulsd		xmm2,	xmm3	
	addsd		xmm7,	xmm2	
	add			r10,	3	
	
	;~ b = (int)double_b
	cvttsd2si  	eax,	xmm5
	movzx		r12,	al
	;~ g = (int)double_g
	cvttsd2si  	eax,	xmm6
	movzx		r13,	al
	;~ r = (int)double_r
	cvttsd2si  	eax,	xmm7
	movzx		r14,	al

	;~ Zmiana piksela w kopii, po kolei jego wartosci: b, g, r
	mov			byte [r11]	,	r12b
	mov			byte [r11+1],	r13b
	mov			byte [r11+2],	r14b	
	add			r11,	3
	
	;~ prevcolorshare = ratio + prevcolorshare - 1.0;
	addsd		xmm1,	xmm0	
	subsd		xmm1,  qword [CONST1]
	
	;~ double_b = (double)ptr1[source_x] * prevcolorshare;
	movzx		r12,	byte [r10]
	cvtsi2sd	xmm5,	r12	
	mulsd		xmm5,	xmm1
	
	;~ double_g 
	movzx		r13,	byte [r10+1]
	cvtsi2sd	xmm6,	r13	
	mulsd		xmm6,	xmm1
	
	;~ double_r
	movzx		r14,	byte [r10+2]
	cvtsi2sd	xmm7,	r14	
	mulsd		xmm7,	xmm1
	
	jmp L2_condition
	
L2_else:
	
	;~ prevcolorshare  += ratio; 
	addsd		xmm1,	xmm0	
	
	;~ double_b = double_b + (double)ptr1[source_x] * ratio;	
	movzx		r12d,	byte [r10]
	cvtsi2sd	xmm2,	r12d	
	mulsd		xmm2,	xmm0
	addsd		xmm5, 	xmm2
	
	;~ double_g 
	movzx		r13d,	byte [r10+1]
	cvtsi2sd 	xmm2,	r13d
	mulsd		xmm2,	xmm0
	addsd		xmm6, 	xmm2
	
	;~ double_r
	movzx		r14d,	byte [r10+2]
	cvtsi2sd	xmm2,	r14d	
	mulsd		xmm2,	xmm0
	addsd		xmm7, 	xmm2
		
	add			r10,	3	
		
L2_condition:
	cmp			r10,	r15
	jl	L2
	;~ ************** KONIEC PETLI L2 ****************	
	
L0_iter:		
	sub			dword line_nr,	dword 1
			 
L0_condition:
	cmp			dword line_nr, dword 0
	jns	L0
	;~ ************** KONIEC PETLI L0 ****************		
	
exit0:
	;~  rax to wartosc zwracana
	mov			rax, 0					
exit:

	pop 		r15
	pop 		r14
	pop 		r13
	pop 		r12	
	pop 		rbx	

	mov			rsp, rbp ; czyszczenie stosu do punktu wyjscia
	pop			rbp
	ret	
