CC = gcc
OBJ = morph.o fmorphing.o
LINKOBJ = morph.o fmorphing.o
BIN = morph
CFLAGS = -Wall 
$(BIN): $(OBJ)
	$(CC) $(LINKOBJ) $(CFLAGS) -o $(BIN) `pkg-config --cflags --libs allegro`
main.o: morph.c
	$(CC) $(CFLAGS) -c morph.c -o morph.o
fmorphing.o: fmorphing.asm
	yasm -f elf64 fmorphing.asm
	
del:
	rm -f morph.o morph fmorphing.o
