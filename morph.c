/*
 * morph.c
 * 
 * Copyright 2013 Jakub Szuppe <j.szuppe@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <allegro.h>

volatile int close_button_pressed = FALSE;

void close_button_handler(void)
{
	close_button_pressed = TRUE;
}
END_OF_FUNCTION(close_button_handler)

void fmorphing(const double a, const double b, const int imgh, const int imgw, unsigned char ** source_line, unsigned char ** copy_line);

void check_ptr(void * const, char *, ...);
void check_ret(const int, char *, ...);

int main(int argc, char **argv)
{
	if (argc != 2) {
      allegro_message("Usage: 'morph yourbitmap.bmp'\n");
      return 1;
	}
	
	check_ret(allegro_init(), "Failed to initialize Allegro! %s", allegro_error);
	check_ret(install_keyboard(), "Failed to install keyboard! %s", allegro_error);
	
	LOCK_FUNCTION(close_button_handler);
    set_close_button_callback(close_button_handler);		
	set_window_title("Morphing App.");		
		
	BITMAP *source_image;	
	PALETTE the_palette;		
		
	source_image = load_bitmap(argv[1], the_palette);				
	const int imgw = source_image->w;
	const int imgh = source_image->h;		
	destroy_bitmap(source_image);	
	
	const int screenw = imgw; 
	const int screenh = imgh;
	 
    set_color_depth(24);
    check_ret(set_gfx_mode(GFX_AUTODETECT_WINDOWED, screenw, screenh, 0, 0), "Failed to set graphics mode! %s", allegro_error);		
			
    
	source_image = load_bitmap(argv[1], the_palette);		
	check_ptr(source_image, "Failed to load %s! %s", argv[1], allegro_error);			
	
	BITMAP *copy_image = create_bitmap(imgw, imgh);
	blit(source_image, copy_image, 0, 0, 0, 0, imgw, imgh);	
	
	double _a = 10;
	double _b = imgw/2;				
			
	blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
	blit(source_image, screen, 0, 0, imgw, 0, imgw, imgh);	
	
	while (!close_button_pressed)
	{
		if (key[KEY_LEFT])
		{
			if (_b >= 1.0)
			{			
				_b -= .5;			
				fmorphing(_a, _b, imgw, imgh, source_image->line, copy_image->line);						
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
			}
		}
		
		if (key[KEY_RIGHT])
		{
			if (_b < imgw)			
			{
				_b += .5;
				fmorphing(_a, _b, imgw, imgh, source_image->line, copy_image->line);			
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
			}
		}
		
		if (key[KEY_DOWN])
		{
			if (_a > 30.0)
			{			
				_a -= 1;
				fmorphing(_a, _b, imgw, imgh, source_image->line, copy_image->line);
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
			}
			else if (_a > 5.0)
			{			
				_a -= 0.1;
				fmorphing(_a, _b, imgw, imgh, source_image->line, copy_image->line);
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
			}
			else if (_a > 0.1)
			{			
				_a -= 0.01;
				fmorphing(_a, _b, imgw, imgh, source_image->line, copy_image->line);
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
			}
		}
		
		if (key[KEY_UP])
		{
			if (_a < 50.0)
			{			
				_a += 0.01;
				fmorphing(_a, _b, imgw, imgh, source_image->line, copy_image->line);
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
			}				
		}
		
		if (key[KEY_ESC])
		{
			break;
		}		
	}
				
	destroy_bitmap(source_image);	
	destroy_bitmap(copy_image);	
	//~ readkey();			
		
	return 0;
}
END_OF_MAIN();

void check_ret(const int ret, char * msg, ...)
{
	if(ret != 0)
	{
		va_list ap;
		va_start(ap, msg);
		
		vfprintf(stderr, msg, ap);
		fputs("\n", stderr);
		
		va_end(ap);
		
		exit(1);
	}
}

void check_ptr(void * const ptr, char * msg, ...)
{
	if(ptr == 0)
	{
		va_list ap;
		va_start(ap, msg);
		
		vfprintf(stderr, msg, ap);
		fputs("\n", stderr);
		
		va_end(ap);
		
		exit(1);
	}
}
